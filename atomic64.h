/* 
 * Copyright (c), 2012 rodneydd, scitecwri, sudolskym
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *	* Names of copyright holders or contributors may not be used to endorse or
 *	  promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS software IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file atomic64.h
 * @brief Header file for x86_64 atomic functions
 */

#if ! defined(__ATOMIC64IH__)
#define __ATOMIC64IH__

#ifndef __x86_64__
#error "Only for __x86_64__"
#endif

#include <stdint.h>

# if defined __cplusplus
extern "C" {
# endif



/*!
 * Atomically increment 8byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_inc64(uint64_t *p)
{
	__asm__ __volatile__("lock; incq %0"
						:"+m"(*p));
}

/*!
 * Atomically decrement 8byte value
 *
 * @param p Pointer from value
 */
static inline void atomic_dec64(uint64_t *p)
{
	__asm__ __volatile__("lock; decq %0"
						:"+m"(*p));
}

/*!
 * Atomically add from 8byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_add64(uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock addq %1, %0"
						:"+m" (*p)
						:"q" (v));
}


/*!
 * Atomically substract from 8byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_sub64(uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock subq %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically and to 8byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_and64(uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock andq %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically or to 8byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_or64(uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock orq %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically xor to 8byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_xor64(uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock xorq %1, %0"
						:"+m" (*p)
						:"q" (v));
}


/*!
 * Atomically exchange and add 64 bit values
 *
 * @param p Pointer to value
 * @param v new 64 bit value
 * @return old new v value
 */
static inline uint64_t atomic_xadd64(uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock xaddq %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}



/*!
 * Atomically test and set bits in a 64 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be set
 * @return old value
 */

static inline uint64_t atomic_test_and_set64(volatile uint64_t * p, uint64_t v)
{
	uint64_t ret;

	__asm__ __volatile__
	(
		"mov %1, %0 \n"
		"1: \n"
		"mov %0, %%rbx \n"
		"or %2, %%rbx \n"
		"lock; cmpxchg %%rbx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "rbx"
	);

	return ret;
}

/*!
 * Atomically test and clear bits in a 64 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be set
 * @return old value
 */

static inline uint64_t atomic_test_and_clear64(volatile uint64_t * p, uint64_t v)
{
	uint64_t ret;

	__asm__ __volatile__
	(
		"mov %1, %0 \n"
		"not %2 \n"
		"1: \n"
		"mov %0, %%rbx \n"
		"and %2, %%rbx \n"
		"lock; cmpxchg %%rbx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "rbx"
	);

	return ret;
}

/*!
 * Atomically test and compliment bits in a 64 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be inverted
 * @return old value
 */

static inline uint64_t atomic_test_and_compliment64(volatile uint64_t * p, uint64_t v)
{
	uint64_t ret;

	__asm__ __volatile__
	(
		"mov %1, %0 \n"
		"1: \n"
		"mov %0, %%rbx \n"
		"xor %2, %%rbx \n"
		"lock; cmpxchg %%rbx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "rbx"
	);

	return ret;
}

/*!
 * Atomically negate 8byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_neg64(int64_t *p)
{
	__asm__ __volatile__("lock; negq %0"
						:"+m"(*p));
}

/*!
 * Atomically not 8byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_not64(int64_t *p)
{
	__asm__ __volatile__("lock; notq %0"
						:"+m"(*p));
}

/*!
 * Atomically write 128 bit value
 *
 * @param p Pointer to value
 * @param v new 128 bit value
 */
static inline void atomic_write128(volatile __int128 *p, __int128 v)
{
	register uint64_t v_hi asm ("rcx") = v >> 64;
	register uint64_t v_lo asm ("rbx") = v;

	__asm__ __volatile__
   (
		/*	We are not interested in the existing value of *p, however,
			it must be moved into rdx:rax or the cmpxchng16b instruction will fail\n */
		"mov (%0), %%rax  \n"
		"mov 0x8(%0), %%rdx\n"
		"1: \n"
		"lock cmpxchg16b (%0) \n"
		"jnz 1b \n"
		: 
		: "r" (p), "r" (v_lo), "r" (v_hi), "m" (*p)
		: "rax", "rdx", "cc");
}

/*!
 * Atomically read a 128 bit value
 *
 * @param p Pointer to value
 * @return v 128 bit value that has been read
 */
static inline __int128 atomic_read128(volatile __int128 *p)
{

	__int128 v;

	__asm__ __volatile__
   (
		/*  Now make sure rcx:rbx=rdx:rax.  In the unlikely event of this
			being equal to *m, this will ensure *m in not modified by the write.
			If they are not equal to *m, no write will occur. */
		"mov %%rax, %%rbx \n"
		"mov %%rdx, %%rcx \n"
		"lock cmpxchg16b (%1) \n"
		: "+A" (v)  /* Specifies the result is in rdx:rax */
		: "r" (p), "m" (*p)
		: "rbx", "rcx", "cc");
	return v;
}

/*!
 * Atomically exchange 128 bit values
 *
 * @param p Pointer to value
 * @param v new 128 bit value
 * @return old value
 */
static inline __int128 atomic_xchg128(volatile __int128 *p, __int128 v)
{
	__int128 ret_v;
	register uint64_t v_hi asm ("rcx") = v >> 64;
	register uint64_t v_lo asm ("rbx") = v;

	__asm__ __volatile__
   (
		/*	Start with a non-atomic read */
		"mov (%1), %%rax  \n"
		"mov 0x8(%1), %%rdx\n"
		"1: \n"
		"lock cmpxchg16b (%1) \n"
		"jnz 1b \n"
		: "+A" (ret_v)
		: "r" (p), "r" (v_lo), "r" (v_hi), "m" (*p)
		: "cc");		

	return ret_v;
}

/*!
 * Atomically test and set a single bit in a 8 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint64_t atomic_bts64(uint64_t *p, uint64_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $63, %2 \n"
						 "lock; btsq %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically test and compliment a single bit in a 8 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint64_t atomic_btc64(uint64_t *p, uint64_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $63, %2 \n"
						 "lock; btcq %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically test and clear a single bit in a 8 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint64_t atomic_btr64(uint64_t *p, uint64_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $63, %2 \n"
						 "lock; btrq %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically compare and exchange 64 bit values
 *
 * @param p Pointer to value
 * @param v new 32 bit value
 * @return old new v value
 */
/*static inline uint64_t atomic_cmpxchg64(volatile uint64_t *p, uint64_t v)
{
	__asm__ __volatile__("lock; cmpxchgq %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}*/

/*!
 * Atomically compare exchange 64 bit values
 *
 * @param p Pointer to value
 * @param cmpval compare value
 * @param v New value if compare succeeds
 * @return old new *p value
 */
static inline uint64_t atomic_cmpxchg64(volatile uint64_t *p, uint64_t cmpval, uint64_t v)
{
	__asm__ __volatile__("lock; cmpxchgq %3, %0"
						:"+m" (*p), "=a" (cmpval)
						:"1" (cmpval), "r" (v));
	return cmpval;
}

# if defined __cplusplus
}
# endif

#endif
