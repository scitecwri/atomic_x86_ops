/* 
 * Copyright (c), 2012 rodneydd, scitecwri, sudolskym
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *	* Names of copyright holders or contributors may not be used to endorse or
 *	  promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS software IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file atomic.h
 * @brief Header file for x86 and x86_64 atomic functions
 */

#if ! defined(__ATOMICIH__)
#define __ATOMICIH__

#include <stdint.h>

# if defined __cplusplus
extern "C" {
# endif

/*!
 * Atomically exchange 8 bit values
 *
 * @param p Pointer to value
 * @param v new 8 bit value
 * @return old value
 */
static inline uint8_t atomic_xchg8(volatile uint8_t *p, uint8_t v)
{
	__asm__ __volatile__("lock; xchgb %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}

/*!
 * Atomically exchange 16 bit values
 *
 * @param p Pointer to value
 * @param v new 16 bit value
 * @return old value
 */
static inline uint16_t atomic_xchg16(volatile uint16_t *p, uint16_t v)
{
	__asm__ __volatile__("lock; xchgw %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}

/*!
 * Atomically exchange 32 bit values
 *
 * @param p Pointer to value
 * @param v new 32 bit value
 * @return old new v value
 */
static inline uint32_t atomic_xchg32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock; xchgl %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}

/*!
 * Atomically compare and exchange 8 bit values
 *
 * @param p Pointer to value
 * @param cmpval - value to compare against
 * @param v new 8 bit value stored if compare successfull
 * @return old value
 */
static inline uint8_t atomic_cmpxchg8(volatile uint8_t *p, uint8_t cmpval, uint8_t v)
{
	__asm__ __volatile__("lock; cmpxchgb %3, %0"
						:"+m" (*p), "=a" (cmpval)
						:"1" (cmpval), "r" (v));
	return cmpval;
}

/*!
 * Atomically compare and exchange 16 bit values
 *
 * @param p Pointer to value
 * @param cmpval - value to compare against
 * @param v new 16 bit value stored if compare successfull
 * @return old value
 */
static inline uint16_t atomic_cmpxchg16(volatile uint16_t *p, uint16_t cmpval, uint16_t v)
{
	__asm__ __volatile__("lock; cmpxchgw %3, %0"
						:"+m" (*p), "=a" (cmpval)
						:"1" (cmpval), "r" (v));
	return cmpval;
}

/*!
 * Atomically compare and exchange 32 bit values
 *
 * @param p Pointer to value
 * @param cmpval - value to compare against
 * @param v new 32 bit value stored if compare successfull
 * @return old value
 */

static inline uint32_t atomic_cmpxchg32(volatile uint32_t *p, uint32_t cmpval, uint32_t v)
{
	__asm__ __volatile__("lock; cmpxchgl %3, %0"
						:"+m" (*p), "=a" (cmpval)
						:"1" (cmpval), "r" (v));
	return cmpval;
}
/*!
 * Atomically increment byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_inc8(volatile uint8_t *p)
{
	__asm__ __volatile__("lock; incb %0"
						:"+m"(*p));
}

/*!
 * Atomically decrement byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_dec8(volatile uint8_t *p)
{
	__asm__ __volatile__("lock; decb %0"
						:"+m"(*p));
}

/*!
 * Atomically increment 2byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_inc16(volatile uint16_t *p)
{
	__asm__ __volatile__("lock; incw %0"
						:"+m"(*p));
}

/*!
 * Atomically decrement 2byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_dec16(volatile uint16_t *p)
{
	__asm__ __volatile__("lock; decw %0"
						:"+m"(*p));
}

/*!
 * Atomically increment 4byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_inc32(volatile uint32_t *p)
{
	__asm__ __volatile__("lock; incl %0"
						:"+m"(*p));
}

/*!
 * Atomically decrement 4byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_dec32(volatile uint32_t *p)
{
	__asm__ __volatile__("lock; decl %0"
						:"+m"(*p));
}

/*!
 * Atomically add to byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_add8(volatile uint8_t *p, uint8_t v)
{
	__asm__ __volatile__("lock addb %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically add to 2byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_add16(volatile uint16_t *p, uint16_t v)
{
	__asm__ __volatile__("lock addw %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically add to 4byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_add32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock addl %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically substract to byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_sub8(volatile uint8_t *p, uint8_t v)
{
	__asm__ __volatile__("lock subb %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically substract to 2byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_sub16(volatile uint16_t *p, uint16_t v)
{
	__asm__ __volatile__("lock subw %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically substract to 4byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_sub32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock subl %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically and to byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_and8(volatile uint8_t *p, uint8_t v)
{
	__asm__ __volatile__("lock andb %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically and to 2byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_and16(volatile uint16_t *p, uint16_t v)
{
	__asm__ __volatile__("lock andw %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically and to 4byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_and32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock andl %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically or to byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_or8(volatile uint8_t *p, uint8_t v)
{
	__asm__ __volatile__("lock orb %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically or to 2byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_or16(volatile uint16_t *p, uint16_t v)
{
	__asm__ __volatile__("lock orw %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically or to 4byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_or32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock orl %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically xor to byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_xor8(volatile uint8_t *p, uint8_t v)
{
	__asm__ __volatile__("lock xorb %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically xor to 2byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_xor16(volatile uint16_t *p, uint16_t v)
{
	__asm__ __volatile__("lock xorw %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically xor to 4byte value
 *
 * @param p Pointer to value
 * @param v value to add
 */
static inline void atomic_xor32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock xorl %1, %0"
						:"+m" (*p)
						:"q" (v));
}

/*!
 * Atomically exchange and add 8 bit values
 *
 * @param p Pointer to value
 * @param v new 8 bit value
 * @return old new v value
 */
static inline uint32_t atomic_xadd8(volatile uint32_t *p, uint8_t v)
{
	__asm__ __volatile__("lock xaddb %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}

/*!
 * Atomically exchange and add 16 bit values
 *
 * @param p Pointer to value
 * @param v new 16 bit value
 * @return old new v value
 */
static inline uint32_t atomic_xadd16(volatile uint32_t *p, uint16_t v)
{
	__asm__ __volatile__("lock xaddw %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}

/*!
 * Atomically exchange and add 32 bit values
 *
 * @param p Pointer to value
 * @param v new 32 bit value
 * @return old new v value
 */
static inline uint32_t atomic_xadd32(volatile uint32_t *p, uint32_t v)
{
	__asm__ __volatile__("lock xaddl %1, %0"
						:"+m" (*p), "+q" (v));
	return v;
}

/*!
 * Atomically write 64 bit value
 *
 * @param p Pointer to value
 * @param v new 64 bit value
 */
static inline void atomic_write64(volatile uint64_t *p, uint64_t v)
{
#ifdef __x86_64__
	/*	On this architecture, a "normal" 64 bit store is atomic */
	* p = v;
#else

	register uint32_t v_hi asm ("ecx") = v >> 32;
	register uint32_t v_lo asm ("ebx") = v;

	__asm__ __volatile__
   (
		/*	We are not interested in the existing value of *p, however,
			it must be moved into edx:eax or the cmpxchng8b instruction will fail\n */
		"mov (%0), %%eax  \n"
		"mov 0x4(%0), %%edx\n"
		"1: \n"
		"lock cmpxchg8b (%0) \n"
		"jnz 1b \n"
		: 
		: "r" (p), "r" (v_lo), "r" (v_hi), "m" (*p)
		: "eax", "edx", "cc");
#endif
}

/*!
 * Atomically read a 64 bit value
 *
 * @param p Pointer to value
 * @return v 64 bit value that has been read
 */
static inline uint64_t atomic_read64(volatile uint64_t *p)
{
#ifdef __x86_64__
	/*	On this architecture, a "normal" 64 bit load is atomic */
	return *p;
#else

	uint64_t v;

	__asm__ __volatile__
   (
		/*  Now make sure ecx:ebx=edx:eax.  In the unlikely event of this
			being equal to *m, this will ensure *m in not modified by the write.
			If they are not equal to *m, no write will occur. */
		"mov %%eax, %%ebx \n"
		"mov %%edx, %%ecx \n"
		"lock cmpxchg8b (%0) \n"
		: "=A" (v) /* Specifies the result is in edx:eax */
		: "r" (p), "m" (*p)
		: "ebx", "ecx", "cc");
	return v;
#endif
}

/*!
 * Atomically exchange 64 bit values
 *
 * @param p Pointer to value
 * @param v new 64 bit value
 * @return old value
 */
static inline uint64_t atomic_xchg64(volatile uint64_t *p, uint64_t v)
{
	uint64_t ret_v;
#ifdef __x86_64__
	__asm__ __volatile__
	(
		"xchg %0, %1"
		: "+m" (*p), "=r" (ret_v)
		: "1" (v)
	);
#else
	register uint32_t v_hi asm ("ecx") = v >> 32;
	register uint32_t v_lo asm ("ebx") = v;

	__asm__ __volatile__
   (
		/*	Start with a non-atomic read */
		"mov (%1), %%eax  \n"
		"mov 0x4(%1), %%edx\n"
		"1: \n"
		"lock cmpxchg8b (%1) \n"
		"jnz 1b \n"
		: "+A" (ret_v)
		: "r" (p), "r" (v_lo), "r" (v_hi), "m" (*p)
		: "cc");		
#endif

	return ret_v;
}

/*!
 * Atomically test and set bits in an 8 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be set
 * @return old value
 */

static inline uint8_t atomic_test_and_set8(volatile uint8_t * p, uint8_t v)
{
	uint8_t ret;

	__asm__ __volatile__
	(
		"movb %1, %0 \n"
		"1: \n"
		"mov %0, %%bl \n"
		"or %2, %%bl \n"
		"lock; cmpxchgb %%bl, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "bl"
	);

	return ret;
}

/*!
 * Atomically test and set bits in a 16 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be set
 * @return old value
 */

static inline uint16_t atomic_test_and_set16(volatile uint16_t * p, uint16_t v)
{
	uint16_t ret;

	__asm__ __volatile__
	(
		"movw %1, %0 \n"
		"1: \n"
		"mov %0, %%bx \n"
		"or %2, %%bx \n"
		"lock; cmpxchgw %%bx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "bx"
	);

	return ret;
}

/*!
 * Atomically test and set bits in a 32 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be set
 * @return old value
 */

static inline uint32_t atomic_test_and_set32(volatile uint32_t * p, uint32_t v)
{
	uint32_t ret;

	__asm__ __volatile__
	(
		"mov %1, %0 \n"
		"1: \n"
		"mov %0, %%ebx \n"
		"or %2, %%ebx \n"
		"lock; cmpxchg %%ebx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "ebx"
	);

	return ret;
}

/*!
 * Atomically test and clear bits in an 8 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be cleared
 * @return old value
 */

static inline uint8_t atomic_test_and_clear8(volatile uint8_t * p, uint8_t v)
{
	uint8_t ret;

	__asm__ __volatile__
	(
		"movb %1, %0 \n"
		"not %2 \n"
		"1: \n"
		"mov %0, %%bl \n"
		"and %2, %%bl \n"
		"lock; cmpxchgb %%bl, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "bl"
	);

	return ret;
}

/*!
 * Atomically test and clear bits in a 16 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be cleared
 * @return old value
 */

static inline uint16_t atomic_test_and_clear16(volatile uint16_t * p, uint16_t v)
{
	uint16_t ret;

	__asm__ __volatile__
	(
		"movw %1, %0 \n"
		"not %2 \n"
		"1: \n"
		"mov %0, %%bx \n"
		"and %2, %%bx \n"
		"lock; cmpxchgw %%bx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "bx"
	);

	return ret;
}

/*!
 * Atomically test and clear bits in a 32 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be cleared
 * @return old value
 */

static inline uint32_t atomic_test_and_clear32(volatile uint32_t * p, uint32_t v)
{
	uint32_t ret;

	__asm__ __volatile__
	(
		"mov %1, %0 \n"
		"not %2 \n"
		"1: \n"
		"mov %0, %%ebx \n"
		"and %2, %%ebx \n"
		"lock; cmpxchg %%ebx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "ebx"
	);

	return ret;
}

/*!
 * Atomically test and compliment bits in an 8 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be inverted
 * @return old value
 */

static inline uint8_t atomic_test_and_compliment8(volatile uint8_t * p, uint8_t v)
{
	uint8_t ret;

	__asm__ __volatile__
	(
		"movb %1, %0 \n"
		"1: \n"
		"mov %0, %%bl \n"
		"xor %2, %%bl \n"
		"lock; cmpxchgb %%bl, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "bl"
	);

	return ret;
}

/*!
 * Atomically test and compliment bits in a 16 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be inverted
 * @return old value
 */

static inline uint16_t atomic_test_and_compliment16(volatile uint16_t * p, uint16_t v)
{
	uint16_t ret;

	__asm__ __volatile__
	(
		"movw %1, %0 \n"
		"1: \n"
		"mov %0, %%bx \n"
		"xor %2, %%bx \n"
		"lock; cmpxchgw %%bx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "bx"
	);

	return ret;
}

/*!
 * Atomically test and compliment bits in a 32 bit number
 *
 * @param p Pointer to value
 * @param v bitmask of bits to be inverted
 * @return old value
 */

static inline uint32_t atomic_test_and_compliment32(volatile uint32_t * p, uint32_t v)
{
	uint32_t ret;

	__asm__ __volatile__
	(
		"mov %1, %0 \n"
		"1: \n"
		"mov %0, %%ebx \n"
		"xor %2, %%ebx \n"
		"lock; cmpxchg %%ebx, %1 \n"
		"jnz 1b \n"
		: "+a" (ret)
		: "m" (*p), "r" (v)
		: "ebx"
	);

	return ret;
}

/*!
 * Atomically negate byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_neg8(int8_t *p)
{
	__asm__ __volatile__("lock; negb %0"
						:"+m"(*p));
}

/*!
 * Atomically negate 2byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_neg16(int16_t *p)
{
	__asm__ __volatile__("lock; negw %0"
						:"+m"(*p));
}

/*!
 * Atomically negate 4byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_neg32(int32_t *p)
{
	__asm__ __volatile__("lock; negl %0"
						:"+m"(*p));
}

/*!
 * Atomically not byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_not8(int8_t *p)
{
	__asm__ __volatile__("lock; notb %0"
						:"+m"(*p));
}

/*!
 * Atomically not 2byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_not16(int16_t *p)
{
	__asm__ __volatile__("lock; notw %0"
						:"+m"(*p));
}

/*!
 * Atomically not 4byte value
 *
 * @param p Pointer to value
 */
static inline void atomic_not32(int32_t *p)
{
	__asm__ __volatile__("lock; notl %0"
						:"+m"(*p));
}


/*!
 * Atomically test and set a single bit in a 2 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint16_t atomic_bts16(uint16_t *p, uint16_t bitnum)
{
	uint8_t retval;

	__asm__ __volatile__("and $15, %2 \n"
						 "lock; btsw %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically test and set a single bit in a 4 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint32_t atomic_bts32(uint32_t *p, uint32_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $31, %2 \n"
						 "lock; btsl %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}


/*!
 * Atomically test and clear a single bit in a 2 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint16_t atomic_btr16(uint16_t *p, uint16_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $15, %2 \n"
						 "lock; btrw %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically test and clear a single bit in a 4 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint32_t atomic_btr32(uint32_t *p, uint32_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $31, %2 \n"
						 "lock; btrl %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically test and compliment a single bit in a 2 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint16_t atomic_btc16(uint16_t *p, uint16_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $15, %2 \n"
						 "lock; btcw %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

/*!
 * Atomically test and compliment a single bit in a 4 byte value
 *
 * @param p Pointer to value
 * @param bitnum - Which bit to set 
 * @return the previous value of the bit
 */
static inline uint32_t atomic_btc32(uint32_t *p, uint32_t bitnum)
{
	uint8_t retval;
	__asm__ __volatile__("and $31, %2 \n"
						 "lock; btcl %2, %0 \n"
						 "setc %1 \n"
						:"+m"(*p), "=r" (retval)
						: "r" (bitnum));

	return retval;
}

# if defined __cplusplus
}
# endif

#endif
