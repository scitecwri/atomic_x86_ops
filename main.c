/* 
 * Copyright (c), 2012 rodneydd, scitecwri, sudolskym
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *	* Names of copyright holders or contributors may not be used to endorse or
 *	  promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS software IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file main.c
 * @brief Test program
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <sched.h>
#include "atomic.h"
#ifdef __x86_64__
#include "atomic64.h"
#endif

#define NUM_TESTING_ROUNDS  1000
#define NUM_TESTING_THREADS 2
#define ATOMIC_CNT_TRIGGER  500
#define ATOMIC_CNT_ROUNDS   3000
#define MAX_TESTING_CYCLES  1000000000

#ifndef false
# define false 0
# define true 1
#endif

#define X8_TYPE  0
#define X16_TYPE 1
#define X32_TYPE 2
#define I8_TYPE  3
#define I16_TYPE 4
#define I32_TYPE 5
#define A8_TYPE  6
#define A16_TYPE 7
#define A32_TYPE 8

typedef uint8_t (*ATOMIC_FUNC_X8)(volatile uint8_t *p, uint8_t v);
typedef uint16_t (*ATOMIC_FUNC_X16)(volatile uint16_t *p, uint16_t v);
typedef uint32_t (*ATOMIC_FUNC_X32)(volatile uint32_t *p, uint32_t v);

typedef void (*ATOMIC_FUNC_I8)(volatile uint8_t *p);
typedef void (*ATOMIC_FUNC_I16)(volatile uint16_t *p);
typedef void (*ATOMIC_FUNC_I32)(volatile uint32_t *p);

typedef void (*ATOMIC_FUNC_A8)(volatile uint8_t *p, uint8_t v);
typedef void (*ATOMIC_FUNC_A16)(volatile uint16_t *p, uint16_t v);
typedef void (*ATOMIC_FUNC_A32)(volatile uint32_t *p, uint32_t v);

#ifdef __x86_64__

#define X64_TYPE 100
#define I64_TYPE 101
#define A64_TYPE 102

typedef uint64_t (*ATOMIC_FUNC_X64)(volatile uint64_t *p, uint64_t v);
typedef void (*ATOMIC_FUNC_I64)(volatile uint64_t *p);
typedef void (*ATOMIC_FUNC_A64)(volatile uint64_t *p, uint64_t v);

#define uintDBL_t __int128
#define MAX_UINTDBL_T 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFLL

static inline __int128 xchg128(volatile __int128 *p, __int128 v)
{
	__int128 a;
	a=*p;
	*p=v;
	return a;
}

static inline void inc64(volatile uint64_t *p)
{
	(*p)++;
}

static inline void dec64(volatile uint64_t *p)
{
	(*p)--;
}

static inline void add64(volatile uint64_t *p, uint64_t v)
{
	*p+=v;
}

static inline void sub64(volatile uint64_t *p, uint64_t v)
{
	*p-=v;
}

static inline void or64(volatile uint64_t *p, uint64_t v)
{
	*p|=v;
}

static inline void and64(volatile uint64_t *p, uint64_t v)
{
	*p&=v;
}

static inline void xor64(volatile uint64_t *p, uint64_t v)
{
	*p^=v;
}

static inline uint64_t xadd64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a;
	a=*p;
	*p+=v;
	return a;
}

static inline void not64(volatile uint64_t *p)
{
	*p=~*p;
}

static inline void neg64(volatile uint64_t *p)
{
	*p=-*p;
}

static inline uint64_t bts64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a=0;
	v=1LL<<(v%64);
	if(*p&v)a=1;
	*p|=v;
	return a;
}

static inline uint64_t btr64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a=0;
	v=1LL<<(v%64);
	if(*p&v)a=1;
	*p&=~v;
	return a;
}

static inline uint64_t btc64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a=0;
	v=1LL<<(v%64);
	if(*p&v)a=1;
	*p^=v;
	return a;
}

static inline uint64_t tas64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a=*p;
	*p|=v;
	return a;
}

static inline uint64_t tar64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a=*p;
	*p&=~v;
	return a;
}

static inline uint64_t tac64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a=*p;
	*p^=v;
	return a;
}

static inline uint64_t cmpxchg64(volatile uint64_t *p, uint64_t c, uint64_t e)
{
	uint64_t a=*p;
	if(a==c)*p=e;
	return a;
}

#else

#define uintDBL_t uint64_t
#define MAX_UINTDBL_T 0xFFFFFFFFFFFFFFFFLL

#endif

static inline uint8_t xchg8(volatile uint8_t *p, uint8_t v)
{
	uint32_t a;
	a=*p;
	*p=v;
	return a;
}

static inline uint16_t xchg16(volatile uint16_t *p, uint16_t v)
{
	uint32_t a;
	a=*p;
	*p=v;
	return a;
}

static inline uint32_t xchg32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a;
	a=*p;
	*p=v;
	return a;
}

static inline uint64_t xchg64(volatile uint64_t *p, uint64_t v)
{
	uint64_t a;
	a=*p;
	*p=v;
	return a;
}

static inline void inc8(volatile uint8_t *p)
{
	(*p)++;
}

static inline void inc16(volatile uint16_t *p)
{
	(*p)++;
}

static inline void inc32(volatile uint32_t *p)
{
	(*p)++;
}

static inline void dec8(volatile uint8_t *p)
{
	(*p)--;
}

static inline void dec16(volatile uint16_t *p)
{
	(*p)--;
}

static inline void dec32(volatile uint32_t *p)
{
	(*p)--;
}

static inline void add8(volatile uint8_t *p, uint8_t v)
{
	*p+=v;
}

static inline void add16(volatile uint16_t *p, uint16_t v)
{
	*p+=v;
}

static inline void add32(volatile uint32_t *p, uint32_t v)
{
	*p+=v;
}

static inline void sub8(volatile uint8_t *p, uint8_t v)
{
	*p-=v;
}

static inline void sub16(volatile uint16_t *p, uint16_t v)
{
	*p-=v;
}

static inline void sub32(volatile uint32_t *p, uint32_t v)
{
	*p-=v;
}

static inline void or8(volatile uint8_t *p, uint8_t v)
{
	*p|=v;
}

static inline void or16(volatile uint16_t *p, uint16_t v)
{
	*p|=v;
}

static inline void or32(volatile uint32_t *p, uint32_t v)
{
	*p|=v;
}

static inline void and8(volatile uint8_t *p, uint8_t v)
{
	*p&=v;
}

static inline void and16(volatile uint16_t *p, uint16_t v)
{
	*p&=v;
}

static inline void and32(volatile uint32_t *p, uint32_t v)
{
	*p&=v;
}

static inline void xor8(volatile uint8_t *p, uint8_t v)
{
	*p^=v;
}

static inline void xor16(volatile uint16_t *p, uint16_t v)
{
	*p^=v;
}

static inline void xor32(volatile uint32_t *p, uint32_t v)
{
	*p^=v;
}

static inline uint8_t xadd8(volatile uint8_t *p, uint8_t v)
{
	uint32_t a;
	a=*p;
	*p+=v;
	return a;
}

static inline uint16_t xadd16(volatile uint16_t *p, uint16_t v)
{
	uint32_t a;
	a=*p;
	*p+=v;
	return a;
}

static inline uint32_t xadd32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a;
	a=*p;
	*p+=v;
	return a;
}

static inline void not8(volatile uint8_t *p)
{
	*p=~*p;
}

static inline void not16(volatile uint16_t *p)
{
	*p=~*p;
}

static inline void not32(volatile uint32_t *p)
{
	*p=~*p;
}

static inline void neg8(volatile uint8_t *p)
{
	*p=-*p;
}

static inline void neg16(volatile uint16_t *p)
{
	*p=-*p;
}

static inline void neg32(volatile uint32_t *p)
{
	*p=-*p;
}

static inline uint16_t bts16(volatile uint16_t *p, uint16_t v)
{
	uint16_t a=0;
	v=1<<(v%16);
	if(*p&v)a=1;
	*p|=v;
	return a;
}

static inline uint32_t bts32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a=0;
	v=1<<(v%32);
	if(*p&v)a=1;
	*p|=v;
	return a;
}

static inline uint16_t btr16(volatile uint16_t *p, uint16_t v)
{
	uint16_t a=0;
	v=1<<(v%16);
	if(*p&v)a=1;
	*p&=~v;
	return a;
}

static inline uint32_t btr32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a=0;
	v=1<<(v%32);
	if(*p&v)a=1;
	*p&=~v;
	return a;
}

static inline uint16_t btc16(volatile uint16_t *p, uint16_t v)
{
	uint16_t a=0;
	v=1<<(v%16);
	if(*p&v)a=1;
	*p^=v;
	return a;
}

static inline uint32_t btc32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a=0;
	v=1<<(v%32);
	if(*p&v)a=1;
	*p^=v;
	return a;
}

static inline uint8_t tas8(volatile uint8_t *p, uint8_t v)
{
	uint8_t a=*p;
	*p|=v;
	return a;
}

static inline uint16_t tas16(volatile uint16_t *p, uint16_t v)
{
	uint16_t a=*p;
	*p|=v;
	return a;
}

static inline uint32_t tas32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a=*p;
	*p|=v;
	return a;
}

static inline uint8_t tar8(volatile uint8_t *p, uint8_t v)
{
	uint8_t a=*p;
	*p&=~v;
	return a;
}

static inline uint16_t tar16(volatile uint16_t *p, uint16_t v)
{
	uint16_t a=*p;
	*p&=~v;
	return a;
}

static inline uint32_t tar32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a=*p;
	*p&=~v;
	return a;
}

static inline uint8_t tac8(volatile uint8_t *p, uint8_t v)
{
	uint8_t a=*p;
	*p^=v;
	return a;
}

static inline uint16_t tac16(volatile uint16_t *p, uint16_t v)
{
	uint16_t a=*p;
	*p^=v;
	return a;
}

static inline uint32_t tac32(volatile uint32_t *p, uint32_t v)
{
	uint32_t a=*p;
	*p^=v;
	return a;
}

static inline uint8_t cmpxchg8(volatile uint8_t *p, uint8_t c, uint8_t e)
{
	uint8_t a=*p;
	if(a==c)*p=e;
	return a;
}

static inline uint16_t cmpxchg16(volatile uint16_t *p, uint16_t c, uint16_t e)
{
	uint16_t a=*p;
	if(a==c)*p=e;
	return a;
}

static inline uint32_t cmpxchg32(volatile uint32_t *p, uint32_t c, uint32_t e)
{
	uint32_t a=*p;
	if(a==c)*p=e;
	return a;
}

typedef struct
{
	char *name;
	int type;
	void *atomic_func;
	void *func;
}FUNC_TAB_TYPE;

FUNC_TAB_TYPE func_tab[]={
	{"xchg8", X8_TYPE, (void*)atomic_xchg8,                (void*)xchg8},
	{"xchg16",X16_TYPE,(void*)atomic_xchg16,               (void*)xchg16},
	{"xchg32",X32_TYPE,(void*)atomic_xchg32,               (void*)xchg32},
	{"inc8",  I8_TYPE, (void*)atomic_inc8,                 (void*)inc8},
	{"inc16", I16_TYPE,(void*)atomic_inc16,                (void*)inc16},
	{"inc32", I32_TYPE,(void*)atomic_inc32,                (void*)inc32},
	{"dec8",  I8_TYPE, (void*)atomic_dec8,                 (void*)dec8},
	{"dec16", I16_TYPE,(void*)atomic_dec16,                (void*)dec16},
	{"dec32", I32_TYPE,(void*)atomic_dec32,                (void*)dec32},
	{"add8",  A8_TYPE, (void*)atomic_add8,                 (void*)add8},
	{"add16", A16_TYPE,(void*)atomic_add16,                (void*)add16},
	{"add32", A32_TYPE,(void*)atomic_add32,                (void*)add32},
	{"sub8",  A8_TYPE, (void*)atomic_sub8,                 (void*)sub8},
	{"sub16", A16_TYPE,(void*)atomic_sub16,                (void*)sub16},
	{"sub32", A32_TYPE,(void*)atomic_sub32,                (void*)sub32},
	{"and8",  A8_TYPE, (void*)atomic_and8,                 (void*)and8},
	{"and16", A16_TYPE,(void*)atomic_and16,                (void*)and16},
	{"and32", A32_TYPE,(void*)atomic_and32,                (void*)and32},
	{"or8",   A8_TYPE, (void*)atomic_or8,                  (void*)or8},
	{"or16",  A16_TYPE,(void*)atomic_or16,                 (void*)or16},
	{"or32",  A32_TYPE,(void*)atomic_or32,                 (void*)or32},
	{"xor8",  A8_TYPE, (void*)atomic_xor8,                 (void*)xor8},
	{"xor16", A16_TYPE,(void*)atomic_xor16,                (void*)xor16},
	{"xor32", A32_TYPE,(void*)atomic_xor32,                (void*)xor32},
	{"xadd8", X8_TYPE, (void*)atomic_xadd8,                (void*)xadd8},
	{"xadd16",X16_TYPE,(void*)atomic_xadd16,               (void*)xadd16},
	{"xadd32",X32_TYPE,(void*)atomic_xadd32,               (void*)xadd32},
	{"not8",  I8_TYPE, (void*)atomic_not8,                 (void*)not8},
	{"not16", I16_TYPE,(void*)atomic_not16,                (void*)not16},
	{"not32", I32_TYPE,(void*)atomic_not32,                (void*)not32},
	{"neg8",  I8_TYPE, (void*)atomic_neg8,                 (void*)neg8},
	{"neg16", I16_TYPE,(void*)atomic_neg16,                (void*)neg16},
	{"neg32", I32_TYPE,(void*)atomic_neg32,                (void*)neg32},
	{"bts16", X16_TYPE,(void*)atomic_bts16,                (void*)bts16},
	{"bts32", X32_TYPE,(void*)atomic_bts32,                (void*)bts32},
	{"btr16", X16_TYPE,(void*)atomic_btr16,                (void*)btr16},
	{"btr32", X32_TYPE,(void*)atomic_btr32,                (void*)btr32},
	{"btc16", X16_TYPE,(void*)atomic_btc16,                (void*)btc16},
	{"btc32", X32_TYPE,(void*)atomic_btc32,                (void*)btc32},
	{"tas8",  X8_TYPE, (void*)atomic_test_and_set8,        (void*)tas8},
	{"tas16", X16_TYPE,(void*)atomic_test_and_set16,       (void*)tas16},
	{"tas32", X32_TYPE,(void*)atomic_test_and_set32,       (void*)tas32},
	{"tar8",  X8_TYPE, (void*)atomic_test_and_clear8,      (void*)tar8},
	{"tar16", X16_TYPE,(void*)atomic_test_and_clear16,     (void*)tar16},
	{"tar32", X32_TYPE,(void*)atomic_test_and_clear32,     (void*)tar32},
	{"tac8",  X8_TYPE, (void*)atomic_test_and_compliment8, (void*)tac8},
	{"tac16", X16_TYPE,(void*)atomic_test_and_compliment16,(void*)tac16},
	{"tac32", X32_TYPE,(void*)atomic_test_and_compliment32,(void*)tac32},
#ifdef __x86_64__
	{"xchg64",X64_TYPE,(void*)atomic_xchg64,               (void*)xchg64},
	{"inc64", I64_TYPE,(void*)atomic_inc64,                (void*)inc64},
	{"dec64", I64_TYPE,(void*)atomic_dec64,                (void*)dec64},
	{"add64", A64_TYPE,(void*)atomic_add64,                (void*)add64},
	{"sub64", A64_TYPE,(void*)atomic_sub64,                (void*)sub64},
	{"and64", A64_TYPE,(void*)atomic_and64,                (void*)and64},
	{"or64",  A64_TYPE,(void*)atomic_or64,                 (void*)or64},
	{"xor64", A64_TYPE,(void*)atomic_xor64,                (void*)xor64},
	{"xadd64",X64_TYPE,(void*)atomic_xadd64,               (void*)xadd64},
	{"not64", I64_TYPE,(void*)atomic_not64,                (void*)not64},
	{"neg64", I64_TYPE,(void*)atomic_neg64,                (void*)neg64},
	{"bts64", X64_TYPE,(void*)atomic_bts64,                (void*)bts64},
	{"btr64", X64_TYPE,(void*)atomic_btr64,                (void*)btr64},
	{"btc64", X64_TYPE,(void*)atomic_btc64,                (void*)btc64},
	{"tas64", X64_TYPE,(void*)atomic_test_and_set64,       (void*)tas64},
	{"tar64", X64_TYPE,(void*)atomic_test_and_clear64,     (void*)tar64},
	{"tac64", X64_TYPE,(void*)atomic_test_and_compliment64,(void*)tac64},
#endif
};

static inline uint64_t fpu_read64(volatile uint64_t *p)
{
	uint64_t v;

	__asm__ __volatile__
   (
		"fildq (%1)\n"
		"fistpq %0\n"
		: "=m" (v)
		: "r" (p)
	);
	return v;
}

static inline void fpu_write64(volatile uint64_t *p,uint64_t v)
{
	__asm__ __volatile__
   (
		"fildq %1\n"
		"fistpq (%0)\n"
		: 
		: "r" (p),"m" (v)
	);
}

typedef struct
{
	uintDBL_t *val;
	int cnt;
	int cycles;
}LOADSTORE_DATA;

void *thread_loadstore_func(void *p)
{
	LOADSTORE_DATA *data=(LOADSTORE_DATA*)p;
	uintDBL_t val;
	for(int i=0;i<data->cycles;i++)
	{
		*data->val=0;
		*data->val=MAX_UINTDBL_T;
		val=*data->val;
		if((val!=0)&&(val!=MAX_UINTDBL_T))data->cnt++;
	}
	return NULL;
}

void *thread_atomic_loadstore_func(void *p)
{
	LOADSTORE_DATA *data=(LOADSTORE_DATA*)p;
	uintDBL_t val;
	for(int i=0;i<data->cycles;i++)
	{
#ifndef __x86_64__
		atomic_write64(data->val,0);
		atomic_write64(data->val,MAX_UINTDBL_T);
		val=atomic_read64(data->val);
#else
		atomic_write128(data->val,0);
		atomic_write128(data->val,MAX_UINTDBL_T);
		val=atomic_read128(data->val);
#endif
		if((val!=0)&&(val!=MAX_UINTDBL_T))data->cnt++;
	}
	return NULL;
}

typedef struct
{
	uintDBL_t *val;
	uintDBL_t pri;
	int cycles;
}XCHG_DATA;

void *thread_xchg_func(void *p)
{
	XCHG_DATA *data=(XCHG_DATA*)p;
	for(int i=0;i<data->cycles;i++)
	{
#ifndef __x86_64__
		data->pri=xchg64(data->val,data->pri);
#else
		data->pri=xchg128(data->val,data->pri);
#endif
	}
	return NULL;
}

void *thread_atomic_xchg_func(void *p)
{
	XCHG_DATA *data=(XCHG_DATA*)p;
	for(int i=0;i<data->cycles;i++)
	{
#ifndef __x86_64__
		data->pri=atomic_xchg64(data->val,data->pri);
#else
		data->pri=atomic_xchg128(data->val,data->pri);
#endif
	}
	return NULL;
}

uint64_t rand64()
{
	return rand()+((uint64_t)rand()<<32);
}

uintDBL_t randDBL()
{
#ifndef __x86_64__
	return rand()+((uintDBL_t)rand()<<32);
#else
	return rand()+((uintDBL_t)rand()<<32)+((uintDBL_t)rand()<<64)+((uintDBL_t)rand()<<96);
#endif
}

int main(int argc,char *argv[])
{
	uint8_t a8,b8,c8,d8;
	uint16_t a16,b16,c16,d16;
	uint32_t a32,b32,c32,d32;
	uint64_t a64,b64,c64,d64;
	uintDBL_t aDBL,bDBL,cDBL,dDBL;
	int passed=true;
	srand(time(NULL));

	for(int i=0;i<sizeof(func_tab)/sizeof(FUNC_TAB_TYPE);i++)
	{
		passed=true;
		printf("Testing %s\t... ",func_tab[i].name);
		fflush(stdout);
		for(int j=0;j<NUM_TESTING_ROUNDS;j++)
		{
			switch(func_tab[i].type)
			{
				case X8_TYPE:
					c8=a8=rand();
					d8=b8=rand();
					b8=((ATOMIC_FUNC_X8)(func_tab[i].atomic_func))(&a8,b8);
					d8=((ATOMIC_FUNC_X8)(func_tab[i].func))(&c8,d8);
					if((a8!=c8)||(b8!=d8))passed=false;
				break;
				case X16_TYPE:
					c16=a16=rand();
					d16=b16=rand();
					b16=((ATOMIC_FUNC_X16)(func_tab[i].atomic_func))(&a16,b16);
					d16=((ATOMIC_FUNC_X16)(func_tab[i].func))(&c16,d16);
					if((a16!=c16)||(b16!=d16))passed=false;
				break;
				case X32_TYPE:
					c32=a32=rand();
					d32=b32=rand();
					b32=((ATOMIC_FUNC_X32)(func_tab[i].atomic_func))(&a32,b32);
					d32=((ATOMIC_FUNC_X32)(func_tab[i].func))(&c32,d32);
					if((a32!=c32)||(b32!=d32))passed=false;
				break;
				case I8_TYPE:
					b8=a8=rand();
					((ATOMIC_FUNC_I8)(func_tab[i].atomic_func))(&a8);
					((ATOMIC_FUNC_I8)(func_tab[i].func))(&b8);
					if(a8!=b8)passed=false;
				break;
				case I16_TYPE:
					b16=a16=rand();
					((ATOMIC_FUNC_I16)(func_tab[i].atomic_func))(&a16);
					((ATOMIC_FUNC_I16)(func_tab[i].func))(&b16);
					if(a16!=b16)passed=false;
				break;
				case I32_TYPE:
					b32=a32=rand();
					((ATOMIC_FUNC_I32)(func_tab[i].atomic_func))(&a32);
					((ATOMIC_FUNC_I32)(func_tab[i].func))(&b32);
					if(a32!=b32)passed=false;
				break;
				case A8_TYPE:
					c8=a8=rand();
					d8=b8=rand();
					((ATOMIC_FUNC_X8)(func_tab[i].atomic_func))(&a8,b8);
					((ATOMIC_FUNC_X8)(func_tab[i].func))(&c8,d8);
					if((a8!=c8)||(b8!=d8))passed=false;
				break;
				case A16_TYPE:
					c16=a16=rand();
					d16=b16=rand();
					((ATOMIC_FUNC_X16)(func_tab[i].atomic_func))(&a16,b16);
					((ATOMIC_FUNC_X16)(func_tab[i].func))(&c16,d16);
					if((a16!=c16)||(b16!=d16))passed=false;
				break;
				case A32_TYPE:
					c32=a32=rand();
					d32=b32=rand();
					((ATOMIC_FUNC_X32)(func_tab[i].atomic_func))(&a32,b32);
					((ATOMIC_FUNC_X32)(func_tab[i].func))(&c32,d32);
					if((a32!=c32)||(b32!=d32))passed=false;
				break;
#ifdef __x86_64__
				case X64_TYPE:
					c64=a64=rand64();
					d64=b64=rand64();
					b64=((ATOMIC_FUNC_X64)(func_tab[i].atomic_func))(&a64,b64);
					d64=((ATOMIC_FUNC_X64)(func_tab[i].func))(&c64,d64);
					if((a64!=c64)||(b64!=d64))passed=false;
				break;
				case I64_TYPE:
					b64=a64=rand64();
					((ATOMIC_FUNC_I64)(func_tab[i].atomic_func))(&a64);
					((ATOMIC_FUNC_I64)(func_tab[i].func))(&b64);
					if(a64!=b64)passed=false;
				break;
				case A64_TYPE:
					c64=a64=rand64();
					d64=b64=rand64();
					((ATOMIC_FUNC_X64)(func_tab[i].atomic_func))(&a64,b64);
					((ATOMIC_FUNC_X64)(func_tab[i].func))(&c64,d64);
					if((a64!=c64)||(b64!=d64))passed=false;
				break;
#endif
			}
		}
		if(passed)printf("passed\n");
		else printf("failed\n");
	}

	printf("Testing cmpxchg8 ... ");
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		uint8_t x8,y8;
		x8=a8=rand();
		b8=rand();
		if(rand()&1)c8=rand();
		else c8=a8;
		d8=atomic_cmpxchg8(&a8,b8,c8);
		y8=cmpxchg8(&x8,b8,c8);
		if((a8!=x8)||(d8!=y8))passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");

	printf("Testing cmpxchg16 ... ");
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		uint16_t x16,y16;
		x16=a16=rand();
		b16=rand();
		if(rand()&1)c16=rand();
		else c16=a16;
		d16=atomic_cmpxchg16(&a16,b16,c16);
		y16=cmpxchg16(&x16,b16,c16);
		if((a16!=x16)||(d16!=y16))passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");

	printf("Testing cmpxchg32 ... ");
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		uint32_t x32,y32;
		x32=a32=rand();
		b32=rand();
		if(rand()&1)c32=rand();
		else c32=a32;
		d32=atomic_cmpxchg32(&a32,b32,c32);
		y32=cmpxchg32(&x32,b32,c32);
		if((a32!=x32)||(d32!=y32))passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");

#ifdef __x86_64__
	printf("Testing cmpxchg64 ... ");
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		uint64_t x64,y64;
		x64=a64=rand64();
		b64=rand64();
		if(rand()&1)c64=rand64();
		else c64=a64;
		d64=atomic_cmpxchg64(&a64,b64,c64);
		y64=cmpxchg64(&x64,b64,c64);
		if((a64!=x64)||(d64!=y64))passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");
#endif

#ifndef __x86_64__
	printf("Testing load64 ... ");
#else
	printf("Testing load128 ... ");
#endif
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		aDBL=randDBL();
#ifndef __x86_64__
		bDBL=atomic_read64(&aDBL);
#else
		bDBL=atomic_read128(&aDBL);
#endif
		if(aDBL!=bDBL)passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");

#ifndef __x86_64__
	printf("Testing store64 ... ");
#else
	printf("Testing store128 ... ");
#endif
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		aDBL=randDBL();
		bDBL=randDBL();
#ifndef __x86_64__
		atomic_write64(&aDBL,bDBL);
#else
		atomic_write128(&aDBL,bDBL);
#endif
		if(aDBL!=bDBL)passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");

#ifndef __x86_64__
	printf("Testing xchg64 ... ");
#else
	printf("Testing xchg128 ... ");
#endif
	fflush(stdout);
	passed=true;
	for(int j=0;j<NUM_TESTING_ROUNDS;j++)
	{
		cDBL=aDBL=randDBL();
		dDBL=bDBL=randDBL();
#ifndef __x86_64__
		bDBL=atomic_xchg64(&aDBL,bDBL);
#else
		bDBL=atomic_xchg128(&aDBL,bDBL);
#endif
		if((aDBL!=dDBL)||(bDBL!=cDBL))passed=false;
	}
	if(passed)printf("passed\n");
	else printf("failed\n");

	int cycles;
	pthread_t tid[NUM_TESTING_THREADS];
	int cnt=0;
	
#ifndef __x86_64__
	printf("Testing atomicity load64/store64 ... ");
#else
	printf("Testing atomicity load128/store128 ... ");
#endif
	fflush(stdout);
	LOADSTORE_DATA ls_data[NUM_TESTING_THREADS];
	cycles=1000;
	cnt=0;
	while((cnt<ATOMIC_CNT_TRIGGER)&&(cycles<MAX_TESTING_CYCLES))
	{
		aDBL=0;
		for(int i=0;i<NUM_TESTING_THREADS;i++)
		{
			ls_data[i].val=&aDBL;
			ls_data[i].cycles=cycles;
			ls_data[i].cnt=0;
			pthread_create(tid+i,NULL,thread_loadstore_func,ls_data+i);
			/*cpu_set_t cs;
			CPU_ZERO_S(sizeof(cpu_set_t),&cs);
			CPU_SET_S(i,sizeof(cpu_set_t),&cs);
			pthread_setaffinity_np(tid[i],sizeof(cpu_set_t),&cs);*/
		}
		for(int i=0;i<NUM_TESTING_THREADS;i++)
			pthread_join(tid[i],NULL);
		cnt=0;
		for(int i=0;i<NUM_TESTING_THREADS;i++)
			cnt+=ls_data[i].cnt;
		cycles*=2;
	}
	if(cnt<ATOMIC_CNT_TRIGGER)printf("error\n");
	else
	{
		aDBL=0;
		for(int i=0;i<NUM_TESTING_THREADS;i++)
		{
			ls_data[i].val=&aDBL;
			ls_data[i].cycles=cycles;
			ls_data[i].cnt=0;
			pthread_create(tid+i,NULL,thread_atomic_loadstore_func,ls_data+i);
		}
		for(int i=0;i<NUM_TESTING_THREADS;i++)
			pthread_join(tid[i],NULL);
		cnt=0;
		for(int i=0;i<NUM_TESTING_THREADS;i++)
			cnt+=ls_data[i].cnt;
		if(cnt==0)printf("passed\n");
		else printf("failed\n");
	}

#ifndef __x86_64__
	printf("Testing atomicity xchg64 ... ");
#else
	printf("Testing atomicity xchg128 ... ");
#endif
	fflush(stdout);
	XCHG_DATA x_data[NUM_TESTING_THREADS];
	cycles=1000;
	cnt=0;
	while((cnt<ATOMIC_CNT_TRIGGER)&&(cycles<MAX_TESTING_CYCLES/ATOMIC_CNT_ROUNDS))
	{
		cnt=0;
		for(int j=0;j<ATOMIC_CNT_ROUNDS;j++)
		{
			aDBL=0;
			for(int i=0;i<NUM_TESTING_THREADS;i++)
			{
				x_data[i].val=&aDBL;
				x_data[i].pri=i+1;
				x_data[i].cycles=cycles;
				pthread_create(tid+i,NULL,thread_xchg_func,x_data+i);
			}
			for(int i=0;i<NUM_TESTING_THREADS;i++)
				pthread_join(tid[i],NULL);
			int vals[NUM_TESTING_THREADS+1];
			for(int i=0;i<NUM_TESTING_THREADS+1;i++)vals[i]=false;
			for(int i=0;i<NUM_TESTING_THREADS;i++)
				vals[x_data[i].pri]=true;
			vals[aDBL]=true;
			passed=true;
			for(int i=0;i<NUM_TESTING_THREADS+1;i++)if(!vals[i])passed=false;
			if(!passed)cnt++;
		}
		cycles*=2;
	}
	if(cnt<ATOMIC_CNT_TRIGGER)printf("error\n");
	else
	{
		cnt=0;
		for(int j=0;j<ATOMIC_CNT_ROUNDS;j++)
		{
			aDBL=0;
			for(int i=0;i<NUM_TESTING_THREADS;i++)
			{
				x_data[i].val=&aDBL;
				x_data[i].pri=i+1;
				x_data[i].cycles=cycles;
				pthread_create(tid+i,NULL,thread_atomic_xchg_func,x_data+i);
			}
			for(int i=0;i<NUM_TESTING_THREADS;i++)
				pthread_join(tid[i],NULL);
			int vals[NUM_TESTING_THREADS+1];
			for(int i=0;i<NUM_TESTING_THREADS+1;i++)vals[i]=false;
			for(int i=0;i<NUM_TESTING_THREADS;i++)
				vals[x_data[i].pri]=true;
			vals[aDBL]=true;
			passed=true;
			for(int i=0;i<NUM_TESTING_THREADS+1;i++)if(!vals[i])passed=false;
			if(!passed)cnt++;
		}
		if(cnt==0)printf("passed\n");
		else printf("failed\n");
	}

	return 0;
}
